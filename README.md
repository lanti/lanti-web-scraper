# lanti-web-scraper

## Prerequisites

* [Node.js runtime with NPM](https://nodejs.org/)
* [Yarn Package Manager](https://yarnpkg.com/)

## Installation & usage

```sh
$ yarn install
$ yarn start
```

Optionally, run this command before install, to keep Babel settings up-to-date (may require you to edit `./src/index.js` afterwards):

```sh
$ npx babel-upgrade --write
```

## Links

```txt
https://www.npmjs.com/package/jsdom
https://github.com/jsdom/jsdom

https://cheerio.js.org/
https://www.npmjs.com/package/cheerio
https://github.com/cheeriojs/cheerio

https://www.npmjs.com/package/puppeteer
https://github.com/GoogleChrome/puppeteer

https://github.com/nwjs/nw.js

----

https://medium.freecodecamp.org/node-js-streams-everything-you-need-to-know-c9141306be93

https://scotch.io/tutorials/web-scraping-scotch-the-node-way
https://www.codeproject.com/Tips/701689/How-to-scrape-data-from-the-Web-using-Node-js
https://dev.to/aurelkurtula/introduction-to-web-scraping-with-nodejs-9h2
https://medium.freecodecamp.org/the-ultimate-guide-to-web-scraping-with-node-js-daa2027dcd3
https://codeburst.io/an-introduction-to-web-scraping-with-node-js-1045b55c63f7
```
