#!/usr/bin/env node

// For airBnB, babel inserting strict mode
require('@babel/register')({
  presets: [
    ['@babel/preset-env', {
      targets: {
        node: '11.1',
      },
    }],
  ],
  plugins: [
    '@babel/plugin-transform-runtime',
    // @babel/preset-stage-3
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-syntax-import-meta',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-json-strings',
  ],
});
require('./app');
