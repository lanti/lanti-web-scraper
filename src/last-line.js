import fs from 'fs';
import readline from 'readline';
import Stream from 'stream';

const getLastLine = (fileName, minLength) => {
  const inStream = fs.createReadStream(fileName);
  const outStream = new Stream();
  return new Promise((resolve, reject) => {
    const rl = readline.createInterface(inStream, outStream);

    let lastLine = '';
    rl.on('line', (line) => {
      if (line.length >= minLength) {
        lastLine = line;
      }
    });

    rl.on('error', reject);

    rl.on('close', () => {
      resolve(lastLine);
    });
  });
};

export default getLastLine;
