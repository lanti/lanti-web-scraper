import fs from 'fs';
import path from 'path';

import fileExists from './file-exists';
import getLastLine from './last-line';

const abs = path.resolve(process.cwd(), '../');
const file = `${abs}/dump/big.file`;
const minLineLength = 1;

console.log(`ABS: ${abs}`);

const readMyData = (filename) => {
  // Create a stream from some character  device.
  const stream = fs.createReadStream(`${filename}`);

  /* stream.on('data', (chunk) => {
    console.log(`Received ${chunk.length} bytes of data.`);
  });
  stream.on('end', () => {
    console.log('There will be no more data.');
  }); */

  setTimeout(() => {
    stream.close(); // This may not close the stream.
    // Artificially marking end-of-stream, as if the underlying resource had
    // indicated end-of-file by itself, allows the stream to close.
    // This does not cancel pending read operations, and if there is such an
    // operation, the process may still not be able to exit successfully
    // until it finishes.
    stream.push(null);
    stream.read(0);
  }, 100);
};

const ifExists = (filename) => {
  // https://nodejs.org/api/fs.html#fs_fs_accesssync_path_mode
  fs.open(filename, 'r', (err, fd) => {
    if (err) {
      if (err.code === 'ENOENT') {
        console.error(`${filename} does not exist`);
        return;
      }
      throw err;
    }

    console.log(fd);
    readMyData(filename);
  });
};

// ifExists(`${abs}/dump/big.file`);

if (fileExists(file)) {
  getLastLine(file, minLineLength)
    .then((lastLine) => {
      console.log(lastLine);
    })
    .catch((err) => {
      console.error(err);
    });
}
