import fs from 'fs';

const fileExists = (filename) => {
  // https://nodejs.org/api/fs.html#fs_fs_accesssync_path_mode
  try {
    fs.accessSync(filename, fs.constants.R_OK | fs.constants.W_OK);
    console.log(`${filename} exists, and it is writable`);
    return true;
  } catch (err) {
    console.error(
      `${filename} ${err.code === 'ENOENT' ? 'does not exist' : 'is read-only'}`,
    );
    return false;
  }
};

export default fileExists;
