// https://eslint.org/docs/user-guide/configuring

const OFF = 0;
const WARN = 1;
// const ERROR = 2;

module.exports = {
  root: true,

  parser: 'babel-eslint',

  plugins: [
    // 'babel',
    'flowtype',
  ],

  extends: [
    'airbnb',
    'plugin:flowtype/recommended',
  ],

  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: true,
    },
  },

  globals: {
    jQuery: false,
    $: false,
  },

  env: {
    // es6: true,
    browser: true,
    node: true,
    jest: true,
    jquery: false,
    serviceworker: true,
  },

  parserOptions: {
    ecmaVersion: 9,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },

  rules: {
    'react/jsx-filename-extension': [WARN, { extensions: ['.js'] }],
    'react/jsx-closing-bracket-location': OFF,
  },
};
